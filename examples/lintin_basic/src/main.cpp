#include <Arduino.h>
#include "lintin.h"
#include "mam_log.h"

Lintin_t lintin = {0};

void *lintin_task_arg;
void *lintin_micro_task_arg;
HardwareSerial *Meter = &Serial2;
void lintin_request_obis_complete_callback(void *arg)
{
  // Lintin_t *_lintin = (Lintin_t *)arg;
  if (lintin.request_control == LINTIN_REQUEST_COMPLETE)
  {
    switch (lintin.task_index)
    {
    case LINTIN_INSTRUMENTATION_TASK:
    {
      ;
    }
    break;
    }
  }
}

void lintin_micro_complete_callback(void *obj, uint8_t micro_task, void *arg)
{

  switch (micro_task)
  {
  case SEARCH_LOAD_PROFILE:
  {
  }
  break;
  }
}

void lintin_timestamp_callback(uint32_t timestamp)
{
}
void setPIN(uint8_t pin, uint8_t status)
{
}
int32_t MAM_USART_sendCharArray(HardwareSerial *me, uint8_t *charToSend, uint16_t size)
{
    return me->write(charToSend, size);
}
void iec62056_21_get_meter_callback(uint32_t serial)
{
}
uint32_t _unix_time_seconds = 0;
uint32_t *get_unix_time_ptr(){
  return &_unix_time_seconds;
  }	
	
void setup()
{
  // put your setup code here, to run once:
  Serial.begin(115200);

  //serial begijn 8e1
  Serial2.begin(9600, SERIAL_7E1, 16, 17);
  struct IEC62056_21_Config config;
  config.send_to_serialport = (send_to_serialport_cb)MAM_USART_sendCharArray;
  config.get_meter_serial_cb = (iec62056_21_get_meter_serial_cb)iec62056_21_get_meter_callback;
  config.port = Meter;
  IEC62056_21_init(&config);

  lintin.request_obis_complete_cb = lintin_request_obis_complete_callback;
  lintin.micro_task_complete_cb = lintin_micro_complete_callback;
  lintin.timestamp_cb = lintin_timestamp_callback;
  lintin.rtc_unix_time = get_unix_time_ptr();
  lintin.set_pin_state_cb = setPIN;
  lintin_init(&lintin);
}

void lintin_rx_service()
{
  lintin_task_manager(&lintin, lintin_task_arg, lintin_micro_task_arg);
  while (Meter->available() > 0)
  {
    lintin.last_charter_recived = millis();
    lintin.buffer_in_len = Meter->readBytes(lintin.buffer_in, sizeof(lintin.buffer_in));
  }
  //		lintin.buffer_in_len += MAM_USART_readBuffer(&Meter, lintin.buffer_in + lintin.buffer_in_len, sizeof(lintin.buffer_in));

  if (((millis() - lintin.last_charter_recived) >= LINTIN_DATA_RECEIVED_TIMEOUT) && (lintin.buffer_in_len > 0))
  {
    lintin_parse_obis(&lintin);
  }
  if (lintin.communication_timeout_counter > LINTIN_RESPONSE_TIMEOUT_MAX_COUNTER)
  {
    MAM_LOG(LOG_ERR, "\r\nSERIAL COMUNICATION ERROR WITH METER\r\n");
    lintin.communication_timeout_counter = 0;
    // mpp_obj.plc_alarm |= (uint8_t) (TO_BIT(serial_comunication_error_indication));
  }
  lintin.timer_counter = millis();
  lintin_request_timeout(&lintin);
  _unix_time_seconds = millis() / 1000;
}

void loop()
{
  // put your main code here, to run repeatedly:
  lintin_rx_service();
}