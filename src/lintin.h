/**
* @file lintin.h
* @author Ing. Ramses Garcia Dilone (ramses.garcia.d@gmai.com)
* @brief Library for Lintin Meters data acquisition using IEC62056-21 protocol.
* @version 0.1
* @date 2021-12-06
*
* @copyright Electrical Equipmet Supplier Copyright (c) 2021
*
*/

#ifndef LINTIN_H_
#define LINTIN_H_

#ifdef __cplusplus
extern "C"
{
	#endif /* __cplusplus */

	#include "stdint.h"
	#include "stdio.h"
	#include "iec62056_21.h"
	#include "lintin_taks.h"
	#include "mam_linked_list.h"

	#define LINTIN_DATA_RECEIVED_TIMEOUT 100
	#define LINTIN_RESPONSE_TIMEOUT 1000
	#define LINTIN_RESPONSE_TIMEOUT_MAX_COUNTER 10
	#define LINTIN_OBIS_SIZE(x) (sizeof(x) / sizeof(x[0]))
	#define LINTIN_GET_LP_KEY(x,y) (( y << 16) | x);
	
	typedef void  (*lintin_request_obis_complete_cb_t)(void *);
	typedef void (*lintin_timestamp_cb_t)(uint32_t);
	typedef void (*lintin_micro_task_complete_cb_t)(void *, uint8_t, void *);
	typedef void (*set_pin_state_cb_t)(uint8_t, uint8_t);
	

	
	

	enum lintin_request_sizes
	{
		INTRUMENTATION_2S_REQUEST_SIZE = 6,
		READINGS_2S_REQUEST_SIZE = 7
	};
	
	typedef enum 
	{
		PASSWORD_P0,
		PASSWORD_P1,
		PASSWORD_P2
	} lintin_password_type_t;

	typedef enum
	{

		LINTIN_REQUEST_FREE,
		LINTIN_REQUEST_BUSY,
		LINTIN_REQUEST_COMPLETE
	} lintin_request_control_t;
	
	
	typedef enum
	{
		Lintin_start_com,
		Lintin_ack_switch,
		Lintin_password,
		Lintin_hands_sheked_passed,
		Lintin_meter_no_response,
	} lintin_hands_shake_t;

	#pragma pack (1)
	
	

	typedef struct
	{
		uint8_t size;
		OBIS_Codes_t obis[10];

	} lintin_task_t;

	typedef struct
	{
		uint8_t meter_serial[16];
		uint8_t request_size;
		uint32_t password;
		uint32_t parameter;
		uint32_t handshaked_baud_rate;
		uint32_t data_exchange_baud_rate;
		IEC62056_21_cmd_t cmd;
		lintin_password_type_t password_type;
		OBIS_Codes_t obis;

	} lintin_obis_request_t;
	
	typedef struct {
		uint16_t len;
		uint8_t buffer[226-12];
		}lintin_load_profile_deltas_t;
	
	typedef struct	 //TODO:  this struct can be change by meter type
	{
		uint32_t datetime;
		uint32_t total_active_energy;
		//uint32_t total_reactive_energy;
		//uint32_t total_active_energy_t1;
		//uint32_t total_reactive_energy_t1;
		//uint32_t total_active_energy_t2;
		//uint32_t total_reactive_energy_t2;
		//uint32_t total_active_energy_t3;
		//uint32_t total_reactive_energy_t3;
		//uint32_t total_active_energy_t4;
		//uint32_t total_reactive_energy_t4;
		
		
	}lintin_load_profile_t;

	typedef struct
	{
		uint8_t communication_timeout_counter;
		uint8_t minute;
		uint8_t task_index;
		uint8_t unix_time_aux;
		uint8_t hands_shaked_tryout;
		uint8_t buffer_in_len;
		uint8_t previous_minute;
		uint8_t request_index;
		uint8_t micro_task_start;
		uint16_t load_profile_index;
		uint16_t load_profile_start_index;
		uint16_t load_profile_limit_index;
		
		uint32_t last_charter_recived;
		uint32_t last_request;
		uint32_t timer_counter;
		uint32_t previous_hands_shake;
		lintin_load_profile_deltas_t * lintin_load_profile_deltas;
		uint32_t *rtc_unix_time;
		lintin_micro_task_t micro_task;
		lintin_tasks_t current_task;
		lintin_hands_shake_t communication_state;
		lintin_request_control_t request_control;
		struct lintin_hash_table_item **obis_response_table;
		lintin_request_obis_complete_cb_t  request_obis_complete_cb;
		lintin_micro_task_complete_cb_t micro_task_complete_cb;
		lintin_timestamp_cb_t   timestamp_cb;
		set_pin_state_cb_t set_pin_state_cb;
		struct IEC62056_21_Response response;
		mam_list_t obis_list_head_node;

		uint8_t buffer_in[256];

	} Lintin_t;

	void lintin_init(Lintin_t *me);
	void *lintin_get_obis_data(Lintin_t *me, lintin_obis_request_t *);
	void *lintin_parse_obis(Lintin_t *me);


	void lintin_hands_shake(Lintin_t *me, lintin_obis_request_t * lintin_obis_request);

	void lintin_request_timeout(Lintin_t *me);
	void lintin_run_tasks(Lintin_t *me);
	uint32_t lintin_get_obis_value(Lintin_t *me, OBIS_Codes_t obis);
	unsigned long Lintin_Convert_Date_Time_To_Unix(unsigned long date_m, unsigned long time_m);
	void lintin_hash_table_clean(Lintin_t *me, OBIS_Codes_t *obis, uint8_t size);
	uint8_t lintin_request_obis(Lintin_t *me, lintin_obis_request_t *req,const OBIS_Codes_t *obis);
	
	OBIS_Codes_t lintin_convert_obis_datetime(OBIS_Codes_t obis);	
void lintin_task_manager(Lintin_t * me, void * arg, void *micro_task_arg);
	void lintin_micro_task_manager(Lintin_t * me, void * arg);
	void lintin_do_micro_task(Lintin_t * me, lintin_micro_task_t micro_task);



	#ifdef __cplusplus
}
#endif /* __cplusplus */
#endif /* LINTIN_H_ */
