/*
* lintin_tasks.c
*
* Created: 14/12/2021 09:08:28 a. m.
*  Author: labees4
*/
#include "lintin_taks.h"
#include "lintin.h"
#include "mam_log.h"
#include "mam_time.h"
//const char * TASK_STR[8] = {"INSTRUMENTATION", "READINGS","COUNTERS","O'CLOCK","DEAMAND", "NO TASK", "NO TASK", "NO TASK"};

const uint8_t lintin_configure_task_for_min[60] = CMD_SCHADULE_METER_DEFAULT();	   //TODO: read from eeprom
uint8_t previous_minute = 0xFF;
uint8_t previous_task_index = 0xFF;
uint8_t task = 0xFF;
uint8_t task_bits = 0xFF;
uint8_t _normal_task= 0;
uint16_t micro_task_index =0;
#define LOAD_PROFILE_RECORDS 720




void lintin_micro_task_manager(Lintin_t * me, void * arg)
{
	if (!me->micro_task_start)
	return;
	
	switch(me->micro_task)
	{
		case ADJUST_METER_DATETIME:
		{
			const OBIS_Codes_t obis[] = LINTIN_WRITE_DATETIME();
			lintin_obis_request_t req = {0};
			req.request_size = LINTIN_OBIS_SIZE(obis);
			req.cmd = W1_CMD;
			req.password_type = PASSWORD_P1;
			req.parameter = *me->rtc_unix_time;
			lintin_request_obis(me,&req,obis);
			
			if (me->request_index == req.request_size) //TODO: revisar se puede mejorar
			{
				me->micro_task_start = 0;
				me->micro_task = 0;
				MAM_LOG(LOG_DBG,"\r\nEND OF MICROTASK [ADJUST_METER_DATETIME]\r\n");
				
				//me->micro_complete_cb(me,me->micro_task,NULL);
			}
			
			
			break;
		}
		
		case SEARCH_LOAD_PROFILE:
		{
			const OBIS_Codes_t obis[] = {Load_Profile_kWh_FFFF};
			lintin_obis_request_t req = {0};
			req.request_size = LINTIN_OBIS_SIZE(obis);
			req.cmd = R3_CMD;
			req.password_type = PASSWORD_P1;
			//uint8_t * params = arg;
			req.parameter = (me->load_profile_index % LOAD_PROFILE_RECORDS) + 1;
			//uint16_t start = 0;
			///uint16_t end = 10;
			lintin_request_obis(me,&req,obis);
			if (me->load_profile_index == me->load_profile_limit_index ) //TODO: revisar se puede mejorar
			{
				me->micro_task_complete_cb(me,me->micro_task,NULL);
			}
			
			break;
		}
		
		case RELAY_ACTION:
		{
			uint8_t * action = (uint8_t *) arg;
			OBIS_Codes_t obis[2];
			lintin_obis_request_t req = {0};
			obis[0] =  (action[0])? Relay_On_3002: Relay_Off_3001;
			obis[1] = IEC_END_COM;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			req.cmd = W6_CMD;
			req.password_type = PASSWORD_P2;
			req.parameter = 0;
			lintin_request_obis(me,&req,obis);
			if (me->request_index == req.request_size) //TODO: revisar se puede mejorar
			{
				me->micro_task_start = 0;
				me->micro_task = 0;
				MAM_LOG(LOG_DBG,"\r\nEND OF MICROTASK [RELAY ACTION: %s]\r\n",(!action[0])? "CLOSE" : "OPEN" );
				
				//me->micro_complete_cb(me,me->micro_task,NULL);
			}
			
		}
		break;
		case DEMAND_CLEAR:
		{
			OBIS_Codes_t obis[] ={ Demand_Clear_2502, IEC_END_COM};
			lintin_obis_request_t req = {0};
			req.cmd = W6_CMD;
			req.password_type = PASSWORD_P2;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			req.parameter = 0;
			
			lintin_request_obis(me,&req,obis);
			
			if (me->request_index == req.request_size) //TODO: revisar se puede mejorar
			{
				me->micro_task_start = 0;
				me->micro_task = 0;
				MAM_LOG(LOG_DBG,"\r\nEND OF MICROTASK DEMAND RESET]\r\n");
				
				//me->micro_complete_cb(me,me->micro_task,NULL);
			}
		}
		break;
		
		
		case SET_TARIFF:
		{
			const OBIS_Codes_t obis[] = {
				Tariff_Day_1_1101,
				Tariff_Day_2_1102,
				Tariff_Day_3_1103,
				Tariff_Day_4_1104,
				Tariff_Day_5_1105,
				Tariff_Day_6_1106,
				Tariff_Day_7_1107,
			IEC_END_COM	 };
			
			uint64_t tariff = 0x19002359;
			lintin_obis_request_t req = {0};
			req.cmd = W6_CMD;
			req.password_type = PASSWORD_P2;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			req.parameter = tariff;
			
			lintin_request_obis(me,&req,obis);
			
			if (me->request_index == req.request_size)
			{
				me->micro_task_start = 0;
				me->micro_task = 0;
				MAM_LOG(LOG_DBG,"\r\nEND OF MICROTASK DEMAND RESET]\r\n");
				//me->micro_complete_cb(me,me->micro_task,NULL);
			}
			
		}
		
		break;
		
		case MICRO_TASK_NONE :
		break;
		
	}
	
}

void lintin_task_manager(Lintin_t * me, void * arg, void *micro_task_arg)
{
	mam_date_time_t timeinfo;
	lintin_obis_request_t req = {0};
	//	char strftime_buf[64];
	//	time_t now = *me->rtc_unix_time;
	mam_fill_date_time(&timeinfo,*me->rtc_unix_time);
	uint8_t * update_all_data_from_meter = (uint8_t *) arg;
	//localtime_r(&now, &timeinfo);
	///	strftime(strftime_buf, sizeof(strftime_buf), "%c", &timeinfo);
	
	if (timeinfo.mm != previous_minute)
	{
		MAM_LOG(LOG_DBG,"\r\nMINUTE CHANGE NEW TASK[%02d-%02d-%02d %02d:%02d:%02d]\r\n",
		timeinfo.d,timeinfo.m,timeinfo.yOff,timeinfo.hh,timeinfo.mm,timeinfo.ss);
		me->task_index = 0;
		_normal_task = 1;
		me->request_index = 0;
		me->hands_shaked_tryout = 0;
		//lintin_linked_list_clean();
		mam_list_clean(&me->obis_list_head_node);
		
		//mam_linked_list_clean(obis_list_head_node);
		previous_minute = timeinfo.mm;
		
		task_bits = lintin_configure_task_for_min[timeinfo.mm];
		if (arg != NULL)
		{
			if (update_all_data_from_meter[0] == 1)
			{
				update_all_data_from_meter[0] = 0;
				task_bits = ALL_TASK_BIT;
			}
		}
	}
	
	if ((me->task_index != previous_task_index ) && (_normal_task  == 1))
	{
		previous_task_index = me->task_index;
		task = ((task_bits & (1 << me->task_index )));
		//
		//(task)?  	MAM_LOG(LOG_DBG,"TASK #[%d]: TASK:[%s]  \r\n",me->task_index,TASK_STR[me->task_index])
		//:   MAM_LOG(LOG_DBG,"TASK #[%d]: TASK:[NO TAKS]  \r\n",me->task_index);
		
	}
	
	
	
	switch (task)
	{
		case INSTRUMENTATION_BIT:
		{
			const OBIS_Codes_t obis[] = LINTIN_2S_INTRUMENTATIONS_OBIS();
			req.cmd = R1_CMD;
			req.password_type = PASSWORD_P1;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			lintin_request_obis(me,&req,obis);
			if (me->request_index == 0)
			{
				me->set_pin_state_cb(1,0);//voltage led on
			}
		}
		break;
		case READING_BIT:
		{
			const OBIS_Codes_t obis[] = LINTIN_2S_READINGS_OBIS();
			req.cmd = R1_CMD;
			req.password_type = PASSWORD_P1;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			lintin_request_obis(me,&req ,obis);
			if (me->request_index == 0)
			{
				me->set_pin_state_cb(2,0);//readings led on
			}
		}
		break;
		case COUNTER_BIT:
		{
			const OBIS_Codes_t obis[] = LINTIN_COUTERS_OBIS();
			req.cmd = R1_CMD;
			req.password_type = PASSWORD_P1;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			lintin_request_obis(me,&req ,obis);
		}
		break;
		case MAX_DEMAND_BIT:
		{
			const OBIS_Codes_t obis[] = LINTIN_2S_DEMAMD_OBIS();
			req.cmd = R1_CMD;
			req.password_type = PASSWORD_P1;
			req.request_size = LINTIN_OBIS_SIZE(obis);
			lintin_request_obis(me,&req ,obis);
		}
		break;
		
		default:
		{
			
			
			if (me->task_index < 8)
			{
				me->task_index++;
			}
			else
			{
				_normal_task = 0;
				lintin_micro_task_manager(me,micro_task_arg);
			}
		}
		
		break;
	}
	
	
}


void lintin_do_micro_task(Lintin_t * me, lintin_micro_task_t micro_task)
{
	me->micro_task = micro_task;
	me->task_index = 0;
	me->request_index = 0;
	me->hands_shaked_tryout = 0;
	me->micro_task_start = 1;
}
