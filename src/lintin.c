/**
* @file lintin.c
* @author Ing. Ramses Garcia Dilone (ramses.garcia.d@gmai.com)
* @brief Library for Lintin Meters data acquisition using IEC62056-21 protocol.
* @version 0.1
* @date 2021-12-06
*
* @copyright Electrical Equipment Supplier Copyright (c) 2021
*
*/

#include "lintin.h"
#include "mam_linked_list.h"
#include "mam_log.h"
#include "mam_time.h"

#define CHARTOINT(arr,start,pos) (((arr[start + pos] - 0x30) * 10) + (arr[pos + 11] - 0x30))


void lintin_init(Lintin_t *me)
{
	me->task_index = 1;
	 mam_list_init(&me->obis_list_head_node);
	
}

void *lintin_get_obis_data(Lintin_t *me, lintin_obis_request_t *lintin_obis_request)
{

	if (me->communication_state < Lintin_hands_sheked_passed)
	{
		lintin_hands_shake(me,lintin_obis_request);
	}
	else
	{
		me->response = IEC62056_21_get_OBIS(lintin_obis_request->cmd,
		lintin_obis_request->obis,
		lintin_obis_request->password_type,
		lintin_obis_request->parameter,
		lintin_obis_request->meter_serial,
		lintin_obis_request->handshaked_baud_rate,
		lintin_obis_request->data_exchange_baud_rate);
	}
	me->last_request = me->timer_counter;
	me->request_control = LINTIN_REQUEST_BUSY;
	return 0;
}

void *lintin_parse_obis(Lintin_t *me)
{
	uint32_t mult = 0;
	uint8_t meter_com_type = 0x20;

	me->response = IEC62056_21_Parser(me->buffer_in, me->buffer_in_len, meter_com_type);

	if (me->response.status == IEC62056_21_OK_RESPONSE)
	{
		if (me->communication_state == Lintin_start_com && me->response.comunication_stage == IEC62056_21_STARED)
		{
			me->communication_state = Lintin_ack_switch;
		}
		else if (me->communication_state == Lintin_ack_switch && me->response.comunication_stage == IEC62056_21_ACK_SWTICH)
		{
			me->communication_state = Lintin_password;
		}
		else if (me->communication_state == Lintin_password && me->response.value == ACK && me->response.previous_obis == IEC_PASSWORD)
		{
			me->communication_state = Lintin_hands_sheked_passed;
			//	MAM_LOG(LOG_DBG,"\r\nLintin_hands_sheked_passed\r\n");
		}

		// MAM_LOG(LOG_DBG,"nraw: %s, -\t status: %d,\t value: %.2f, obis: %d, pre-obis: %d,  com: %d\t\r\n", me->response.raw_response, me->response.status, (float)me->response.value, me->response.obis, me->response.previous_obis, me->response.comunication_stage);

		if (me->response.previous_obis == Meter_Time_0_9_1 || me->response.previous_obis == Meter_Date_0_9_2) // | me->response.previous_obis == Meter_Date
		{
			//Serial.println("OBIS->Time"); // Dynamic search '('
			unsigned char ind;
			for (ind = 0; ind <= 10; ind++)
			{
				if (me->response.raw_response[ind] == '(')
				{
					break;
				}
			}

			me->response.value = ((me->response.raw_response[ind + 7] - 0x30) << 4) | (me->response.raw_response[ind + 8] - 0x30);           // Day
			me->response.value |= ((me->response.raw_response[ind + 4] - 0x30) << 12) | ((me->response.raw_response[ind + 5] - 0x30) << 8);  // Month
			me->response.value |= ((me->response.raw_response[ind + 1] - 0x30) << 20) | ((me->response.raw_response[ind + 2] - 0x30) << 16); // Year
			
			

		}
		
		else if (me->response.previous_obis == Balance_Day_C_72_0)
		{
			unsigned char ind;
			for (ind = 0; ind <= 10; ind++)
			{
				if (me->response.raw_response[ind] == '(')
				{
					break;
				}
			}

			me->response.value = ((me->response.raw_response[ind + 3] - 0x30) << 4) | (me->response.raw_response[ind + 4] - 0x30);
			me->response.value |= ((me->response.raw_response[ind + 1] - 0x30) << 20) | ((me->response.raw_response[ind + 2] - 0x30) << 8);
		}
		else if ((me->response.previous_obis & 0xFFFFF000) == Active_Total_Max_Demand_1_6_0)
		{
			//MAM_LOG(LOG_DBG,"OBIS->Demand");

			//find '('
			unsigned char P_Pos = 0;
			for (unsigned char P_Index = 0; P_Index < 10; P_Index++)
			{
				if (me->response.raw_response[P_Index] == '(')
				{
					P_Pos = P_Index + 1; //6+1=7
					P_Index = 10;        // End
				}
			}

			mam_date_time_t timex = {0};
			
			/*
			timex.ss = ((me->response.raw_response[P_Pos + 10] - 0x30) * 10) + (me->response.raw_response[P_Pos + 11] - 0x30);
			timex.mm = ((me->response.raw_response[P_Pos + 8] - 0x30) * 10) + (me->response.raw_response[P_Pos + 9] - 0x30);
			timex.hh = (((me->response.raw_response[P_Pos + 6] - 0x30) * 10) + (me->response.raw_response[P_Pos + 7] - 0x30));
			timex.d = (((me->response.raw_response[P_Pos + 4] - 0x30) * 10) + (me->response.raw_response[P_Pos + 5] - 0x30));
			timex.m = (((me->response.raw_response[P_Pos + 2] - 0x30) * 10) + (me->response.raw_response[P_Pos + 3] - 0x30));          //(0-11)
			timex.yOff = ((((me->response.raw_response[P_Pos] - 0x30) * 10) + (me->response.raw_response[P_Pos + 1] - 0x30))); //from 1900
			*/
			
			timex.ss = CHARTOINT(me->response.raw_response,P_Pos,10);
			timex.mm =	CHARTOINT(me->response.raw_response,P_Pos,8);
			timex.hh = CHARTOINT(me->response.raw_response,P_Pos,6);
			timex.d  = CHARTOINT(me->response.raw_response,P_Pos,4);
			timex.m = CHARTOINT(me->response.raw_response,P_Pos,2);
			timex.yOff = CHARTOINT(me->response.raw_response,P_Pos,0);
			P_Pos += 12; //19
			mam_make_unixtime(&timex);
			me->response.value_datetime = timex.unixtime;


			if (me->response.value_datetime <= 1508000000) // if date is all zeroes
			me->response.value_datetime = 0;

			//lintin_linked_list_insert_request(lintin_convert_obis_datetime(me->response.previous_obis),me->response.value_datetime);
			
			mam_list_insert_front(&me->obis_list_head_node,lintin_convert_obis_datetime(me->response.previous_obis),me->response.value_datetime);

			mult = 1;
			for (unsigned char ind = (P_Pos + 8); ind >= P_Pos; ind--)
			{
				if ((me->response.raw_response[ind] <= 0x39) && (me->response.raw_response[ind] >= 0x30)) // avoid '.' 0000.0000
				{
					me->response.value += (me->response.raw_response[ind] - 0x30) * mult;
					mult *= 10;
				}
			}
			
		}
		else if (me->response.previous_obis == Load_Profile_kWh_FFFF)
		{
			
			unsigned char aux_ind, aux_ind_limit;
			unsigned char value_ind, value_ind_limit;
			uint32_t ll_key = 0;
			lintin_load_profile_t * lintin_load_profile = (lintin_load_profile_t * ) malloc(sizeof(lintin_load_profile_t));
			lintin_load_profile->datetime = 0;
			lintin_load_profile->total_active_energy = 0;
			//lintin_load_profile->total_active_energy_t1 = 0;
			//lintin_load_profile->total_reactive_energy = 0;
			//lintin_load_profile->total_reactive_energy_t1 = 0;

			if ((meter_com_type & 0x20) == 0x20)		 //22 01 06 13 00 00
			//if (METER_TYPE_SWITCH == 1) // 0x02 FFFF ((19 05 06 12 45 00)(000000.00)(000000.00)
			//                                      AA MM DD HH MM SS
			{
				aux_ind = 16;
				aux_ind_limit = 9;
				value_ind = 29;
				value_ind_limit = 21;
			}
			else // 0x02 FFFF ((05061245)(000000.00)(000000.00)
			//           MM DD HH MM
			{
				aux_ind = 14;
				aux_ind_limit = 7;
				value_ind = 25;
				value_ind_limit = 17;
			}

			mult = 1;
			for (; aux_ind >= aux_ind_limit; aux_ind--)
			{
				if (me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39)
				{
					lintin_load_profile->datetime += (me->response.raw_response[aux_ind] - 0x30) * mult;
					mult *= 10;
				}
			}
			mult = 1;

			/* if (METER_TYPE_SWITCH == 1)                                    // 17100208 missing :00 hours in 1 phase
			parser_values_struct.Aux = (parser_values_struct.Aux * 100); // Add missing :00 hours in 1 phase Meter
			*/
			for (; value_ind >= value_ind_limit; value_ind--) // Total Active energy
			{
				if (me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39)
				{
					lintin_load_profile->total_active_energy += (me->response.raw_response[value_ind] - 0x30) * mult;
					mult *= 10;
				}
			}
			/*
			if ((meter_com_type & 0x20) == 0x20)
			{
			value_ind = 40;
			value_ind_limit = 32;
			}
			else //if (METER_TYPE_SWITCH == 3)
			{
			value_ind = 36;
			value_ind_limit = 28;
			}

			mult = 1;
			for (; value_ind >= value_ind_limit; value_ind--) // T1 Active energy
			{
			if (me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39)
			{
			lintin_load_profile->total_active_energy_t1 += (me->response.raw_response[value_ind] - 0x30) * mult;
			mult *= 10;
			}
			}
			
			

			///////////////////////////////////////////////////////////
			//R3FFFF(0002)c2
			//
			//FFFF((02241700)(000054.15)(000054.15)(000000.00)(000000.00)(000000.00))
			// if (((int)meter_com_type & 0x20) == 0x20)
			// {
			//   value_ind = 40;
			//   value_ind_limit = 32;
			// }
			// else //if (METER_TYPE_SWITCH == 3)
			// {
			//   value_ind = 47;
			//   value_ind_limit = 39;
			// }

			// mult = 1;
			// for (; value_ind >= value_ind_limit; value_ind--) // T3 Active energy
			// {
			//   if (me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39)
			//   {
			//     me->response.value2 += (me->response.raw_response[value_ind] - 0x30) * mult;
			//     mult *= 10;
			//   }
			// }

			////////////////////////////////////////////////////
			//if (((int)meter_com_type & 0x20) == 0x20)
			//{
			//value_ind = 51;
			//value_ind_limit = 43;
			//}
			//else //if (METER_TYPE_SWITCH == 3)
			//{
			//value_ind = 58;
			//value_ind_limit = 50;
			//}

			//mult = 1;
			//for (; value_ind >= value_ind_limit; value_ind--) // T3 Active energy
			//{
			//if (me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39)
			//{
			////  me->response.value3 += (me->response.raw_response[value_ind] - 0x30) * mult;
			//mult *= 10;
			//}
			//}


			value_ind = 80;
			value_ind_limit = 72;

			mult = 1;
			for( ; value_ind >= value_ind_limit ; value_ind--) // Total Reactive energy
			{
			if ( me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39 )
			{
			lintin_load_profile->total_reactive_energy += ( me->response.raw_response[value_ind] - 0x30 ) * mult;
			mult *= 10;
			}
			}

			value_ind = 91;
			value_ind_limit = 83;

			mult = 1;
			for( ; value_ind >= value_ind_limit ; value_ind--) // T1 Reactive energy
			{
			if ( me->response.raw_response[value_ind] >= 0x30 && me->response.raw_response[value_ind] <= 0x39 )
			{
			lintin_load_profile->total_reactive_energy_t1 += ( me->response.raw_response[value_ind] - 0x30 ) * mult;
			mult *= 10;
			}
			}
			*/
			ll_key = LINTIN_GET_LP_KEY(me->response.previous_obis,me->load_profile_index);
			//MAM_LOG(LOG_DBG,"\r\n[%x]-Parse Load Profile: (%lu) (%lu) (%lu) (%lu) (%lu) \r\n:",ll_key,
			//lintin_load_profile->datetime,
			//lintin_load_profile->total_active_energy,
			//lintin_load_profile->total_active_energy_t1,
			//lintin_load_profile->total_reactive_energy,
			//lintin_load_profile->total_reactive_energy_t1);
			
			//lintin_linked_list_insert_request(ll_key,(uint32_t)lintin_load_profile);
			mam_list_insert_front(&me->obis_list_head_node,ll_key,(uint32_t)lintin_load_profile);
			me->load_profile_index--;
		}
		else if ((me->response.previous_obis & 0x0000FFF0) == Tariff_Day_x_110x)
		{
			me->response.value = ((me->response.raw_response[8] - 0x30) << (4 * 7)) |
			((me->response.raw_response[9] - 0x30) << (4 * 6)) |
			((me->response.raw_response[10] - 0x30) << (4 * 5)) |
			((me->response.raw_response[11] - 0x30) << (4 * 4)) |
			((me->response.raw_response[14] - 0x30) << (4 * 3)) |
			((me->response.raw_response[15] - 0x30) << (4 * 2)) |
			((me->response.raw_response[16] - 0x30) << (4 * 1)) |
			((me->response.raw_response[17] - 0x30));

			//MAM_LOG(LOG_DBG,"me->response.value: %8X \r\n", me->response.value);
			//me->response.previous_obis = Lintin_Response_App.previous_OBIS;
		}
		else if (me->response.previous_obis == 0x00 || me->response.previous_obis == 0x02) // me->response.raw_response[3] == 'E' && me->response.raw_response[4] == 'R'  //ERror
		{
			
		}
		else if (((me->response.previous_obis& Active_Total_Energy_15_8_0) == Active_Total_Energy_15_8_0) ||
		((me->response.previous_obis& Reactive_Total_Energy_16_8_0) == Reactive_Total_Energy_16_8_0) ||
		me->response.previous_obis== Neg_A_Total_Energy_2_8_0)
		{
			me->response.value*= 10; // Add 3rd digit after .

			//MAM_LOG(LOG_DBG,"\r\nOBIS: %8X-%d\r\n", parser_values_struct.OBIS, parser_values_struct.Value);
		}
		
		//MAM_LOG(LOG_DBG,"%d", lookup(t, 5));
		//   MAM_LOG(LOG_DBG,"raw: %s\r\n",(char *)me->buffer_in);
		
		if ((me->communication_state == Lintin_hands_sheked_passed) && (me->response.previous_obis >= Tariff_Day_x_110x) && ( me->response.previous_obis != Load_Profile_kWh_FFFF))
		{
			//lintin_linked_list_insert_request(me->response.previous_obis,me->response.value);
			mam_list_insert_front(&me->obis_list_head_node, me->response.previous_obis,me->response.value);
			
			me->request_index++;
			
			//	MAM_LOG(LOG_DBG,"\r\nobis: %lu, value: %lu\r\n",me->response.previous_obis,me->response.value);
			
		}
		else if(me->communication_state == Lintin_hands_sheked_passed && me->response.previous_obis == IEC_END_COM)
		{
			me->request_index =0;
			

		}
		me->request_control = LINTIN_REQUEST_FREE;
		me->buffer_in_len = 0;
		//  MAM_LOG(LOG_DBG,"  obis: %x\r\n %s", me->response.previous_obis, me->response.raw_response);
		

		
	}
	else if (me->response.status == IEC62056_21_ERROR_RESPONSE)
	{
		
		me->response.status = IEC62056_21_WAIT_RESPONSE;
		me->communication_state = Lintin_start_com;
		me->buffer_in_len = 0;
	}
	
	
	
	
}
void lintin_hands_shake(Lintin_t *me, lintin_obis_request_t * lintin_obis_request)
{

	if(( me->communication_state == 0 ) && (me->hands_shaked_tryout <= 5 ) && (me->timer_counter - me->previous_hands_shake >= 1000))
	{
		me->previous_hands_shake = me->timer_counter;
		IEC62056_21_get_OBIS(R1_CMD,
		IEC_START_COM,
		lintin_obis_request->password_type,
		lintin_obis_request->password,
		lintin_obis_request->meter_serial[0],
		lintin_obis_request->handshaked_baud_rate,
		lintin_obis_request->data_exchange_baud_rate);
		me->hands_shaked_tryout++;
	}
	else if (me->communication_state == 1)
	{
		IEC62056_21_get_OBIS(R1_CMD,
		IEC_ACK_SWITCH,
		lintin_obis_request->password_type,
		lintin_obis_request->password,
		lintin_obis_request->meter_serial[0],
		lintin_obis_request->handshaked_baud_rate,
		lintin_obis_request->data_exchange_baud_rate);
	}
	else if (me->communication_state == 2)
	{
		
		IEC62056_21_get_OBIS(R1_CMD,
		IEC_PASSWORD,
		lintin_obis_request->password_type,
		lintin_obis_request->password,
		lintin_obis_request->meter_serial[0],
		lintin_obis_request->handshaked_baud_rate,
		lintin_obis_request->data_exchange_baud_rate);
	}

}

void lintin_request_timeout(Lintin_t *me)
{
	if ((me->request_control == LINTIN_REQUEST_BUSY) && ( me->request_index != 255))
	{
		if ((me->timer_counter - me->last_request) >= LINTIN_RESPONSE_TIMEOUT)
		{
			me->communication_state = Lintin_start_com;
			me->request_control = LINTIN_REQUEST_FREE;
			me->communication_timeout_counter++;		   			
		}
	}
}


unsigned long Lintin_Convert_Date_Time_To_Unix(unsigned long date_m, unsigned long time_m)
{
	mam_date_time_t timex;
	timex.d = ((((date_m & 0xF0) >> 4 * 1) * 10) + (date_m & 0x0F));
	date_m = date_m >> 8;
	timex.m = (((((date_m & 0xF0) >> 4 * 1) * 10) + (date_m & 0x0F))) & 0xFF;
	date_m = date_m >> 8;
	timex.yOff = (((((date_m & 0xF0) >> 4 * 1) * 10) + (date_m & 0x0F)));
	timex.ss = ((((time_m & 0xF0) >> 4 * 1) * 10) + (time_m & 0x0F));
	time_m = time_m >> 8;
	timex.mm = ((((time_m & 0xF0) >> 4 * 1) * 10) + (time_m & 0x0F));
	time_m = time_m >> 8;
	timex.hh = ((((time_m & 0xF0) >> 4 * 1) * 10) + (time_m & 0x0F));

	mam_make_unixtime(&timex);
	return timex.unixtime;
}






uint8_t lintin_request_obis(Lintin_t * const me, lintin_obis_request_t *req , const OBIS_Codes_t *obis)
{
	if (me->request_control == LINTIN_REQUEST_FREE)
	{
		if (me->request_index < req->request_size)
		{
			if (me->communication_state == Lintin_hands_sheked_passed)
			{
				req->obis = obis[me->request_index];
			}
			
			if(obis[me->request_index] == IEC_END_COM)
			{
				me->request_index++;
			}
			lintin_get_obis_data(me,req);
		}
		else if (me->request_index == req->request_size)
		{
			
			//uint32_t meter_date = lintin_linked_list_get_obis(Meter_Date_0_9_2);
			//uint32_t meter_time = lintin_linked_list_get_obis(Meter_Time_0_9_1);
			
			uint32_t meter_date = mam_linked_list_get_node_val_and_del(&me->obis_list_head_node,Meter_Date_0_9_2);
			uint32_t meter_time = mam_linked_list_get_node_val_and_del(&me->obis_list_head_node,Meter_Time_0_9_1);
			uint32_t timestamp =  Lintin_Convert_Date_Time_To_Unix(meter_date, meter_time);
			//lintin_linked_list_insert_request(METER_TIMESTAMP_OF_REQUEST,timestamp);
			mam_list_insert_front(&me->obis_list_head_node,METER_TIMESTAMP_OF_REQUEST,timestamp);
			me->timestamp_cb(timestamp);
			MAM_LOG(LOG_DBG,"->REQUEST TIMESTAMP: %lu\r\n", timestamp);
			
			me->request_index = 255;
			me->request_control = LINTIN_REQUEST_COMPLETE;
				//lintin_linked_list_print();
			mam_list_print(&me->obis_list_head_node);
			me->request_obis_complete_cb(me);
			//	mam_list_print(&obis_list_head_node);
			//	lintin_linked_list_print();
			me->communication_timeout_counter = 0;
			MAM_LOG(LOG_DBG,"\r\nEND OF REQUESTS\r\n");
			
			
		}
		
		
	}
}



OBIS_Codes_t lintin_convert_obis_datetime(OBIS_Codes_t obis){
	
	
	return obis | 0x0D;
}