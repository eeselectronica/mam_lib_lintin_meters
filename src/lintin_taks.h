/*
* lintin_taks.h
*
* Created: 14/12/2021 02:25:11 p. m.
*  Author: labees4
*/


#ifndef LINTIN_TAKS_H_
#define LINTIN_TAKS_H_
#ifdef __cplusplus
extern "C"
{
	#endif /* __cplusplus */
	#include <stdio.h>
	#include <stdlib.h>

	#define INSTRUMENTATION_BIT   		(1 << 0)
	#define READING_BIT 				(1 << 1)
	#define COUNTER_BIT 				(1 << 2)
	#define MAX_DEMAND_BIT 				(1 << 3)
	//#define O_CLOCK_REGISTERS_BIT 		(1 << 4)
	#define ALL_TASK_BIT				 0x0F

	
	typedef enum
	{
		LINTIN_INSTRUMENTATION_TASK,
		LINTIN_READING_TASK,
		LINTIN_COUNTER_TASK,
		LINTN_MAX_DEMAND_TASK,
	}lintin_tasks_t;
	
	typedef enum
	{
		MICRO_TASK_NONE,
		ADJUST_METER_DATETIME,
		SEARCH_LOAD_PROFILE,
		RELAY_ACTION,
		DEMAND_CLEAR,
		SET_TARIFF,
		
	}lintin_micro_task_t;
	

	#define LINTIN_WRITE_DATETIME() \
	{								\
		Meter_Date_0_9_2,			\
		Meter_Time_0_9_1,			\
		IEC_END_COM					\
	}

	#define LINTIN_2S_INTRUMENTATIONS_OBIS()\
	{										\
		Meter_Date_0_9_2,					\
		Meter_Time_0_9_1,					\
		Total_Power_Factor_13_7_0,			\
		L1_Voltage_32_7_0,					\
		L1_Current_31_7_0,					\
		IEC_END_COM							\
	}

	#define LINTIN_COUTERS_OBIS()   \
	{                               \
		Meter_Date_0_9_2,           \
		Meter_Time_0_9_1,           \
		Meter_Clear_Times_C_66_0,   \
		Power_Off_Times_C_7_5,      \
		Event_Clear_Times_C_69_0,   \
		Demand_Clear_Times_C_67_0,  \
		Cover_Opening_Times_C_50_0, \
		IEC_END_COM                 \
	}

	#define LINTIN_2S_READINGS_OBIS()  \
	{                                  \
		Meter_Date_0_9_2,              \
		Meter_Time_0_9_1,              \
		Active_Total_Energy_15_8_0,    \
		Active_Tariff_1_Energy_15_8_1, \
		Neg_A_Total_Energy_2_8_0,      \
		Reactive_Total_Energy_16_8_0,  \
		IEC_END_COM                    \
	}

	#define LINTIN_2S_DEMAMD_OBIS()		  \
	{                                     \
		Meter_Date_0_9_2,                 \
		Meter_Time_0_9_1,                 \
		Active_Total_Max_Demand_1_6_0,    \
		Active_Tariff_1_Max_Demand_1_6_1, \
		Active_Tariff_3_Max_Demand_1_6_3, \
		IEC_END_COM                       \
	}



	#define CMD_SCHADULE_METER_DEFAULT()                                                                                        \
	{																															\
		ALL_TASK_BIT, INSTRUMENTATION_BIT, COUNTER_BIT,  INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,						/*<min 00 - 04> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 05 - 09> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 10 - 14> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 15 - 19> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 20 - 24> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 25 - 29> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 30 - 34> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 35 - 39> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 40 - 44> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 45 - 49> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT,				/*<min 50 - 54> */ \
		READING_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT, INSTRUMENTATION_BIT					/*<min 55 - 59> */ \
	}


	
	 
	 #define CMD_SCHADULE_METER_DEFAULT_ALL()                                                                                        \
	 {																															\
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,  ALL_TASK_BIT, ALL_TASK_BIT,						/*<min 00 - 04> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 05 - 09> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 10 - 14> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 15 - 19> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 20 - 24> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 25 - 29> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 30 - 34> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 35 - 39> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 40 - 44> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 45 - 49> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT,				/*<min 50 - 54> */ \
		 ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT, ALL_TASK_BIT					/*<min 55 - 59> */ \
	 }

	#ifdef __cplusplus
}
#endif

#endif /* LINTIN_TAKS_H_ */